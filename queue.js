let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    // collection.length = collection.length + 1
    collection[collection.length] = element
    return collection
}

function dequeue() {
    // In here you are going to remove the last element in the array
    
    collection[0] = collection[collection.length - 1]
    collection.length -= 1;
    return collection
}

function front() {
    // In here, you are going to remove the first element
    // collection[collection.length - 1]
    // // collection.length -= 1;
    // return collection
    let firstElement = collection[0]
    return firstElement
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements
    let  count = 0;
    for(let size in collection){
        count+=1
    }
   return count
}

function isEmpty() {
    //it will check whether the function is empty or not
    if (collection === []) {
        return true
    } else {
        return false
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};